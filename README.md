Homepage/Wiki
=============
<https://marvelution.atlassian.net/wiki/display/BAMSON>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/BAMSON>

Continuous Builder
==================
<https://marvelution.atlassian.net/builds/browse/BAMSON>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
