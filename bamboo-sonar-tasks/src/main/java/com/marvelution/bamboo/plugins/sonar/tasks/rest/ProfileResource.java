/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.tasks.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.marvelution.gadgets.sonar.wsclient.connectors.BasicAuthenticationHttpClient4Connector;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.sonar.wsclient.Host;
import org.sonar.wsclient.Sonar;
import org.sonar.wsclient.connectors.HttpClient4Connector;

import com.atlassian.bamboo.rest.entity.RestResponse;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.bamboo.plugins.sonar.tasks.rest.resources.Profile;
import com.marvelution.bamboo.plugins.sonar.tasks.rest.resources.Profiles;
import com.marvelution.bamboo.plugins.sonar.tasks.servers.SonarServer;
import com.marvelution.bamboo.plugins.sonar.tasks.servers.SonarServerManager;
import com.marvelution.bamboo.plugins.sonar.wsclient.services.CustomProfileUnmarshaller;
import com.marvelution.bamboo.plugins.sonar.wsclient.services.ProfileListQuery;
import com.sun.jersey.spi.resource.Singleton;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.5.5
 */
@Path("/profiles")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
public class ProfileResource {

	private static final Logger LOGGER = Logger.getLogger(ProfileResource.class);

	private final CustomProfileUnmarshaller unmarshaller = new CustomProfileUnmarshaller();
	private final SonarServerManager serverManager;
	private final I18nResolver i18nResolver;

	/**
	 * Constructor
	 *
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param i18nResolver the {@link I18nResolver} implementation
	 */
	public ProfileResource(SonarServerManager serverManager, I18nResolver i18nResolver) {
		this.serverManager = serverManager;
		this.i18nResolver = i18nResolver;
	}

	/**
	 * {@link POST} endpoint to get all the profiles
	 * 
	 * @param baseUrl the Sonar base URL
	 * @param username the username of the user to authenticate as
	 * @param password the password of the given user
	 * @param sonarId the ID of a {@link SonarServer}
	 * @return {@link Set} of all the Nexus Staging Profiles
	 */
	@POST
	public Response getProfiles(@FormParam("baseUrl") String baseUrl, @FormParam("username") String username,
					@FormParam("password") String password, @FormParam("sonarId") int sonarId) {
		RestResponse.Builder builder = RestResponse.builder();
		if (sonarId > 0) {
			final SonarServer server = serverManager.getServer(sonarId);
			if (server == null) {
				return Response.ok(
					builder.error(i18nResolver.getText("sonar.rest.invalid.server.id")).build(RestResponse.class))
					.build();
			}
			baseUrl = server.getHost();
			username = server.getUsername();
			password = server.getPassword();
		}
		if (StringUtils.isBlank(baseUrl)) {
			return Response.ok(
				builder.error(i18nResolver.getText("sonar.rest.no.base.url")).build(RestResponse.class)).build();
		}
		try {
			new URI(baseUrl);
			Sonar sonar = new Sonar(new BasicAuthenticationHttpClient4Connector(new Host(baseUrl, username, password)));
			String json = sonar.getConnector().execute(new ProfileListQuery());
			if (StringUtils.isNotBlank(json)) {
				List<org.sonar.wsclient.services.Profile> profiles = unmarshaller.toModels(json);
				Profiles response = new Profiles();
				response.setData(Lists.transform(profiles, new Function<org.sonar.wsclient.services.Profile, Profile>() {
					@Override
					public Profile apply(org.sonar.wsclient.services.Profile from) {
						return Profile.get(from);
					}
				}));
				return Response.ok(response).build();
			} else {
				return Response.ok(
					builder.error(i18nResolver.getText("sonar.rest.no.profiles")).build(RestResponse.class)).build();
			}
		} catch (URISyntaxException e) {
			return Response.ok(
				builder.error(i18nResolver.getText("sonar.rest.malformed.base.url")).error(e.getMessage())
					.build(RestResponse.class)).build();
		} catch (Exception e) {
			LOGGER.debug(e);
			return Response.ok(
				builder.error(i18nResolver.getText("sonar.rest.unexpected.error")).error(e.getMessage())
					.build(RestResponse.class)).build();
		}
	}

}
