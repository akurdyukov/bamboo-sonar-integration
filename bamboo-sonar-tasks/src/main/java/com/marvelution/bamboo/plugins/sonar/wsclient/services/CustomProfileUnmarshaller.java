/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.wsclient.services;

import org.sonar.wsclient.services.Profile;
import org.sonar.wsclient.services.WSUtils;
import org.sonar.wsclient.unmarshallers.AbstractUnmarshaller;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since VERSION
 */
public class CustomProfileUnmarshaller extends AbstractUnmarshaller<Profile> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Profile parse(Object json) {
		WSUtils utils = WSUtils.getINSTANCE();
	    Profile profile = new Profile();
	    profile.setLanguage(utils.getString(json, "language"))
	    	.setName(utils.getString(json, "name"))
	    	.setDefaultProfile(utils.getBoolean(json, "default"));
	    if (utils.getString(json, "parent") != null) {
	    	profile.setParentName(utils.getString(json, "parent"));
	    }
	    if (utils.getBoolean(json, "provided") != null) {
	    	profile.setProvided(utils.getBoolean(json, "provided"));
	    }
		return profile;
	}

}
