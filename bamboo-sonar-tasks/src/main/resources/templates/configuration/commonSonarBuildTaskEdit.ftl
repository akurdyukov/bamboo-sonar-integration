[#--
  ~ Licensed to Marvelution under one or more contributor license
  ~ agreements.  See the NOTICE file distributed with this work
  ~ for additional information regarding copyright ownership.
  ~ Marvelution licenses this file to you under the Apache License,
  ~ Version 2.0 (the "License"); you may not use this file except
  ~ in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~  http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied. See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  --]

[@ui.bambooSection titleKey='sonar.extra.configuration']
	[@ww.select labelKey='sonar.profile' name='sonarProfile' id='sonarProfile' fieldClass='sonar-profile']
	    [@ww.param name='disabled' value=!(sonarProfile?has_content) /]
	    [@ww.param name='extraUtility']
	    	[@ui.displayButton id='sonar-load-profiles' valueKey='sonar.load.profiles'/]
	    [/@ww.param]
	    [#if sonarProfile?has_content]
	        [@ww.param name='headerKey2' value=sonarProfile /]
	        [@ww.param name='headerValue2' value=sonarProfile /]
	    [/#if]
	[/@ww.select]
	[@ww.textfield labelKey='sonar.language' name='sonarLanguage' /]
	[@ww.textfield labelKey='sonar.java.source' name='sonarJavaSource' /]
	[@ww.textfield labelKey='sonar.java.target' name='sonarJavaTarget' /]
	[@ww.textarea labelKey='sonar.custom.extra.parameters' name='sonarExtraCustomParameters' rows='4' /]
[/@ui.bambooSection]

<script type="text/javascript">
(function () {
    var taskForm = new BAMBOO.SONAR.TaskForm({
        selectors: {
        	sonar: 'select[name="sonarId"]',
        	sonarUrl: 'input[name="sonarHostUrl"]',
            username: 'input[name="sonarHostUsername"]',
            password: 'input[name="sonarHostPassword"]',
            profile: 'select[name="sonarProfile"]',
            language: 'input[name="sonarLanguage"]'
        }
    });
    taskForm.init();
}());
</script>

