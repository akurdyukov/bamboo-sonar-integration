/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.tasks.processors;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.SimpleLogEntry;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.google.common.collect.Maps;
import com.marvelution.bamboo.plugins.sonar.tasks.utils.PluginHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link SonarBuildPasswordProcessor}
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.6.1
 */
@RunWith(MockitoJUnitRunner.class)
public class SonarBuildPasswordProcessorTest {

	private static final String COMMAND = "mvn sonar:sonar -Dsonar.login=admin -Dsonar.password=sonar -Dsonar" +
			".jdbc.username=sonar -Dsonar.jdbc.password=sonar";
	private static final String FILTERED = "mvn sonar:sonar -Dsonar.login=admin -Dsonar.password=****** -Dsonar" +
			".jdbc.username=sonar -Dsonar.jdbc.password=******";

	private SonarBuildPasswordProcessor processor;
	@Mock
	private BuildContext buildContext;
	@Mock
	private CurrentBuildResult buildResult;
	Map<String, String> customBuildData;

	/**
	 * Setup the {@link SonarBuildPasswordProcessor} and all the required {@link Mock} objects
	 */
	@Before
	public void setup() {
		processor = new SonarBuildPasswordProcessor();
		customBuildData = Maps.newHashMap();
		when(buildContext.getBuildResult()).thenReturn(buildResult);
		when(buildResult.getCustomBuildData()).thenReturn(customBuildData);
	}

	/**
	 * Test {@link com.marvelution.bamboo.plugins.sonar.tasks.processors.SonarBuildPasswordProcessor#call()}
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testCall() throws Exception {
		String key = SonarBuildPasswordProcessor.BUILD_COMMANDLINE + PluginHelper.getPluginKey();
		customBuildData.put(key, COMMAND);
		processor.init(buildContext);
		BuildContext context = processor.call();
		verify(buildContext, times(2)).getBuildResult();
		verify(buildResult, times(2)).getCustomBuildData();
		assertThat(customBuildData.get(key), is(FILTERED));
		assertThat(context.getBuildResult().getCustomBuildData().get(key), is(FILTERED));
	}

	/**
	 * Test {@link SonarBuildPasswordProcessor#intercept(com.atlassian.bamboo.build.LogEntry)}
	 */
	@Test
	public void testIntercept() {
		LogEntry entry = new SimpleLogEntry(COMMAND);
		processor.intercept(entry);
		assertThat(entry.getLog(), is(FILTERED));
	}

	/**
	 * Test {@link SonarBuildPasswordProcessor#interceptError(com.atlassian.bamboo.build.LogEntry)}
	 */
	@Test
	public void testInterceptError() {
		LogEntry entry = new SimpleLogEntry(COMMAND);
		processor.intercept(entry);
		assertThat(entry.getLog(), is(FILTERED));
	}

}
